1- If the project is in its initial development stage, prior to first working version, commits can be done directly on master branch. 
2- A feature_branch should be created when developing a new feature.
3- Only merge a feature_branch in the master after the feature has been tested.
4- Local commits should be rebase before pushing.
5- To update a local feature branch that has not been pushed to the global server yet with Master branches updates use "git rebase master". This result in a more linear history than "git merge master". Do not do this with a feature branch that is already in the global server.